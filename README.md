# <img src="https://i.ibb.co/CBVnG3C/logo2.png" width="50" height="50"> PufferPanel Storage Limiter v1

PPSL is a python script that allows you to set a maximum allowed storage capacity for game services hosted via the PufferPanel management application. You may download PufferPanel [here](https://docs.pufferpanel.com/en/2.x/).

## [1] Features
- Set a maximum size each game service can use (e.g.: 30GB, 10GB and so on).
- Disable server when the storage limit is exceeded. This means that the game server will shut down and the server cannot be started again. However, server files will still be accessible via SFTP.
- Automatically re-enable server once the user has lowered their storage usage below the set threshold.
- Notifications are sent to the server's console on: (1) reaching 90% of allowed storage capacity, (2) having reached 100% of allowed storage capacity and the server being locked, (3) unlocking server when user has reduced storage usage below the set threshold.

## [2] Requirements

You will need the following to run this script:
- PufferPanel installed, obviously.
- Sudo permission (either with root or preferably with a user account that can sudo without entering a password, needed for chmod)
- Redis server ([Click here](https://tecadmin.net/how-to-install-redis-on-debian-11/) for installation guide) (this is used to cache server states, more features will be added later)
- Python3 (the script was written in Python 3.8, using other versions may or may not break the script's functionality)
- Your PufferPanel Administration account's OAuth2 client id and secret.

## [3] Installation

- Clone this repo to your PufferPanel node that has game services run on it.
- Install the dependencies with pip. (cd folder -> ``pip install -r requirements.txt``)
- Populate the configs.json file with the correct parameters.
- Create a crontab to ppslimiter.py (e.g.: on debian11, run checks every 30m: crontab -e -> */30 * * * * python3 path/to/where/you/downloaded/ppslimiter.py)

Done. :) If your script is working you should see a log file created in /logs folder once the script has ran at least once.

## [4] Configuration

You need to set the correct parameters in the configs.json file **prior** to running the script. Invalid configuration may lead to the script not working as intended.
| Config option | Default value | Description |
| --- | --- | --- |
| REDIS_IP | "127.0.0.1" | This is the IP address of your redis server. Depending on where you have your redis db will change this value. |
| REDIS_PORT | "6379" | This is the port of your redis server. It is 6379 by default, but set it to the appropriate port, if you have changed your redis server's port.
| REDIS_PASS | "" | Password for accessing your redis server. It is empty by default, so please create a strong password prior to starting this script in redis.conf -> requirepass "password" |
| DOMAIN | "panel.example.com" | Domain name used to access your server. In case you're not using a domain, use the IP:PORT of your PufferPanel host machine. |
| CLIENT_ID | "" | Your Administrator account's OAuth2 client id. (Account -> OAuth2 Clients -> Create -> Client ID) |
| CLIENT_SECRET | "" | Your Administrator account's OAuth2 client secret. (^See above -> Client Secret) |
| DEFAULT_LIMIT | 30 | Maximum amount of storage each PufferPanel game service can use in GB.|
| PATH | "/var/lib/pufferpanel/servers" | Path to where your PufferPanel game services' files are stored. This is /var/lib/pufferpanel/servers by default. |

## [5] Important notes

- You need to set this script up on each node that you run game services on using PufferPanel. For example: Node0 only hosts the PufferPanel website, Node1 hosts all the game servers. Therefore you install this script on Node1 because Node0 doesn't have any game servers running on it.
- Setting custom storage limit for each server is **not yet available**, but is in the works. This script assumes you want to set the same storage limitation for each game service regardless of environment or template.
- You should regularly run this script via crontab to automatically check, disable and re-enable services. It is optimal to set the cronjob to run every 10-30 minutes.

## [6] Contact

If you have an issue or would like to ask a question, you can contact me using one of the following:
- Discord: blackruby#2562
- Email: admin@blackruby.hu

Thank you for using this script, any constructive feedback is more than welcome! :)
