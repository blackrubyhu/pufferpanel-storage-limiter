# PufferPanel Storage Limiter | Author: blackruby#2562 | Email: admin@blackruby.hu

import os
import json
import redis
import requests
from requests.structures import CaseInsensitiveDict
import logging
from datetime import datetime

def initialize():
    global config, redis_conn, redis_host, redis_port, redis_pass, domain, client_id, client_secret, path, default_limit, token

    with open(f'{os.path.dirname(os.path.abspath(__file__))}/configs.json') as configs:
        config = json.load(configs)

    redis_host = config["REDIS_IP"]                     # Default port if it is on the same machine as the panel: 127.0.0.1
    redis_port = config["REDIS_PORT"]                   # Default port: 6379
    redis_pass = config["REDIS_PASS"]                   # "requirepass {password}" in redis.conf
    domain = config["DOMAIN"]                           # The domain for the panel without https:// eg.: panel.example.com
    client_id = config["CLIENT_ID"]                     # Users -> OAuth2 Clients -> Create
    client_secret = config["CLIENT_SECRET"]
    path = config["PATH"]                               # Path to where pufferpanel stores server files (default: /var/lib/pufferpanel/servers)
    default_limit = float(config["DEFAULT_LIMIT"])      # Limit (in GB) the each server can use.
    redis_conn = connect_to_redis(redis_host, redis_port, redis_pass)
    token = get_token()

def connect_to_redis(ip, port, passw):
    return redis.Redis(host = ip, port = port, password = passw)

def check_status_code(code, error_message):
    if(code != 204 and code != 200):
        logging.info(f"{error_message} Status code: {code}")
        quit()

def get_token():
    url = f"https://{domain}/oauth2/token"

    headers = CaseInsensitiveDict()
    headers["Content-Type"] = "application/x-www-form-urlencoded"

    data = f"grant_type=client_credentials&client_id={client_id}&client_secret={client_secret}"
    resp = requests.post(url, headers=headers, data=data)

    check_status_code(resp.status_code, f"An error has orrured while trying to get an access token. Are the credentials correct? Is the panel online?")
    return json.loads(resp.content)["access_token"]

def kill_server(server, access_token):
    url = f"https://{domain}/proxy/daemon/server/{server}/kill?wait=false"

    headers = CaseInsensitiveDict()
    headers["accept"] = "application/json"
    headers["Authorization"] = f"Bearer {access_token}"
    headers["Content-Length"] = "0"
    resp = requests.post(url, headers=headers)
    check_status_code(resp.status_code, f"An error has occured while trying to kill this server: {server}. Is the panel and server online?")

def send_message(server, access_token, message):
    url = f"https://{domain}/proxy/daemon/server/{server}/console"

    headers = CaseInsensitiveDict()
    headers["Authorization"] = f"Bearer {access_token}"
    headers["Content-Type"] = "text/xml; charset=utf-8"

    data = message.encode('utf-8')
    resp = requests.post(url, headers=headers, data=data)
    check_status_code(resp.status_code, f"An error has occured while trying to send a message to this server's console: {server}. Is the panel and server online?")

def get_size(start_path):
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(start_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            # skip if it is symbolic link
            if not os.path.islink(fp):
                total_size += os.path.getsize(fp)

    return total_size

def is_in_database(server):
    return redis_conn.exists(server) == 1

def set_state(server, state):
    redis_conn.set(server, state)

def get_state(server):
    return redis_conn.get(server)

def lock_server(path, server):
    os.system(f"sudo chmod -R 455 {path}/{server}")
    logging.info(f"Server with the id {server} is now locked for exceeding the storage capacity of {default_limit}.")

def unlock_server(path, server):
    os.system(f"sudo chmod -R 775 {path}/{server}")
    logging.info(f"Server with the id {server} is now unlocked.")

def run_check():
    root, dirs, files = next(os.walk(path))
    logging.info("PUFFERPANEL STORAGE CHECK IN PROGRESS...")
    logging.info("SERVER ID\t|\tMAX STORAGE\t|\tSTORAGE USED")

    for d in dirs:
        size = round((get_size(f'{path}/{d}') / 1073741824), 2)
        logging.info(f"{d}\t|\t{default_limit} GB\t\t|\t{size} GB")
        if(size < default_limit and is_in_database(d)):
            if(get_state(d) == "locked"):
                unlock_server(path, d)
                send_message(d, token, "[⚠️] Your server is now unlocked. You may start it with the Start button.")
            redis_conn.delete(d)
        if(size >= (default_limit * 0.9)):
            set_state(d, "alert")
            send_message(d, token, f"[⚠️] You have reached 90% of your storage capacity ({default_limit} / {size} GB)! If you exceed your storage capacity, your server will be shut down and frozen until you lower your storage usage. You will still have access to download your files.")
            logging.info(f"Server with the id {d} has exceeded 90% of it's allowed storage capacity. ({size})")
        if(size >= default_limit and get_state(d) == "alert"):
            logging.info(f"Server with the id {d} has reached 100% of it's allowed storage capacity. Locking server in progress...")
            kill_server(d, token)
            lock_server(path, d)
            set_state(d, "locked")
            send_message(d, token, f"[⚠️] You have reached 100% of your allowed storage capacity ({default_limit} / {size} GB)! Your server has been disabled until you lower your storage usage. Once that is done, your server will be unlocked automatically. You can still download all your files.")

if __name__ == "__main__":
    logging.basicConfig(filename=f"{os.path.dirname(os.path.abspath(__file__))}/logs/{datetime.now().strftime('%Y-%m-%d')}.log", level=logging.INFO, format='[%(asctime)s] %(message)s', datefmt='%Y/%m/%d %I:%M:%S %p')

    try:
        initialize()
    except Exception as e:
        logging.info("An error has occured while trying to run the script. Please check the following: 1) your panel is up, 2) your redis db is online, 3) the configs.json file is not empty and is populated with correct credentials.")
        quit()
    run_check()
